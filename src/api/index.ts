import { async } from "q";
import { StockData, MarkovStats } from "../components/Entities";

interface IStockAPI {
  getListOfStocks(): Promise<string[]>;
  getStockData(stockName: string): Promise<StockData>;
  getMarkovStats(stockName: string): Promise<MarkovStats>;
}
export const StockAPI: IStockAPI = {
  async getListOfStocks() {
    let response = await fetch('http://localhost:5000/getstocklist' , {
      method: 'GET',
      credentials: 'same-origin',
    });
    if (!response.ok) {
      console.error('List of stocks has error', response);
      throw `${response}`;
    } else {
      let json: Promise<string[]> = await response.json();
      console.log('json: ', json);
      return json;
    }
  },
  async getStockData(stockName: string) {
    let response = await fetch('http://localhost:5000/data/' + stockName, {
      method: 'GET',
      credentials: 'same-origin',
    });
    console.log('getStockData response: ', response);
    if (!response.ok) {
      console.error('Getting stock info for ' + stockName + ' failed', response);
      throw `${response}`;
    } else {
      let json: Promise<StockData> = await response.json();
      console.log('json: ', json);
      return json;
    }
  },
  async getMarkovStats(stockName: string) {
    let response: Response = await fetch('http://localhost:5000/markov/' + stockName, {
      method: 'GET',
      credentials: 'same-origin',
    });
    console.log('response: ', response)
    if (!response.ok) {
      console.error('Error exercising markov for ' + stockName + ', ', response);
      throw `${response}`;
    } else {
      let json = await response.json();
      return json;
    }
  }
};

  