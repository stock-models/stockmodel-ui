import React from 'react';
import '../style/App.css';
import { Button } from '@blueprintjs/core';

import { StockChart } from './charts';
import { StockAPI } from '../api'
import { StockData, MarkovStats } from './Entities';

interface AppProps {
  displayText: string;
}

interface AppState {
  stockList: string[];
  currentlySelectedStock: string;
  stockData?: StockData;
  markovData?: MarkovStats;
}


export class App extends React.Component<AppProps, AppState> {

  constructor(props: AppProps) {
    super(props);
    this.state = {
      stockList: [],
      currentlySelectedStock: '',
      stockData: undefined,
      markovData: undefined,
    }
  }

  componentDidUpdate(prevProps: AppProps, prevState: AppState) {
    if (prevState.currentlySelectedStock !== this.state.currentlySelectedStock) {
      this.updateStockData();
    }
  }

  componentDidMount() {
    this.loadStockNames();
  }

  render() {
    return (
      <div className="App bp3">
        <h1>
          Please select a stock to view:
        </h1>
        {this.getStockSelector()}
        <br />
        {this.getStockPlotButton()}
        {this.getMarkovButton()}


        <div>
          <StockChart stockData={this.state.stockData} stockName={this.state.currentlySelectedStock} />
        </div>

        {this.showMarkovResults()}
      </div>

    );
  }

  private getStockPlotButton = () => {
    return (
      <Button icon="chart" >
        Plot stock info
      </Button>
    )
  }

  private getMarkovButton = () => {
    return (
      <Button className="pt-dark-button" onClick={this.exerciseMarkov(this.state.currentlySelectedStock)} >
        Test Markov algorithm on selected data set
      </Button>
    );
  }

  private getStockSelector = () => {
    return (
      <select onChange={this.onSelectorChange}>
        {this.getStockSelectorOptions()}
      </select>

    );
  }

  private getStockSelectorOptions = () => {
    let options: JSX.Element[] = [];
    for (let stock of this.state.stockList) {
      let option = (
        <option value={stock}>{stock}</option>
      );
      options.push(option);
    }
    return options;
  }

  private onSelectorChange = (e: React.SyntheticEvent) => {
    let target: any = e.target;
    let targetValue = target.value;
    this.setState({
      currentlySelectedStock: targetValue,
    });
  }

  private exerciseMarkov = (stockName: string) => (e?: React.MouseEvent) => {
    StockAPI.getMarkovStats(stockName).then(
      res => {
        this.setState({
          markovData: res,
        });
      }
    )
  }

  private loadStockNames = (e?: React.MouseEvent) => {
    StockAPI.getListOfStocks().then(
      res => {
        let stockList: string[] = [];
        for (let str of res) {
          stockList.push(str);
        }
        this.setState({
          stockList,
        })
      });
  }

  private updateStockData = () => {
    StockAPI.getStockData(this.state.currentlySelectedStock).then(
      res => {
        this.setState({
          stockData: res,
        });
      }
    )
  }

  private showMarkovResults = () => {
    if (!this.state.markovData) {
      return;
    }
    return (
      <div>
        <p>
          Markov results for {this.state.markovData.name}:
      </p>
        <p>
          Total correct percent: {(this.state.markovData.num_correct / this.state.markovData.num_total).toFixed(2)}%
      </p>
        <p>
          High confidence correct percent: {(this.state.markovData.num_high_conf_correct / this.state.markovData.num_high_conf_total).toFixed(2)}%
      </p>
      </div>
    );
  }
}

