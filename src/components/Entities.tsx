import { number } from "prop-types";
import { Color } from "csstype";

export interface ChartData {
    id: string,
    data: ChartDatum[],
    color? : Color,
}

export interface ChartDatum {
    x: number | Date,
    y: number,
}

export interface StockData {
    "Date" : {
        [index : number] : Date,
    },
    "Open" : {
        [index: number] : number,
    },
}

export interface MarkovStats {
    "name" : string,
    "num_correct" : number,
    "num_total" : number,
    "num_high_conf_correct" : number,
    "num_high_conf_total" : number,
}