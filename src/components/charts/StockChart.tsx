import React from 'react';
import { VictoryLine, VictoryChart, VictoryTheme } from 'victory';
import { ResponsiveLine, LineSerieData } from '@nivo/line';
import { NonIdealState } from '@blueprintjs/core';
import { StockData, ChartData, ChartDatum } from '../Entities';

interface StockChartProps {
  stockName: string;
  stockData?: StockData;
}


export class StockChart extends React.Component<StockChartProps, {}> {

  render() {
    if (!this.props.stockData) {
      return (
        <NonIdealState
          icon={'chart'}
          title={'Select a stock to see its trends'}
        />
      );
    }
    let data: number[] = Object.values(this.props.stockData.Open);
    return (
      <div className="stock" style={{height: 400}}>
        {this.getResponsiveLine(this.props.stockData, this.props.stockName)}
      </div>
    );
  }


  private getResponsiveLine = (stockData: StockData, stockName: string) => {
    console.log('stockData : ' , stockData)
    let data: ChartData = {} as ChartData;
    data.id = stockName;
    data.data = [];
    data.color = "hsl(215, 70%, 50%)";
    for (let i = 0; i < Object.values(stockData.Open).length; i++) {
      data.data.push({
        x: i,
        y: stockData.Open[i],
      });
    }
    return (
      <ResponsiveLine
        data={[data as LineSerieData]}
        margin={{ top: 50, right: 160, bottom: 50, left: 60 }}
        xScale={{ type: 'linear' }}
        yScale={{ type: 'linear', stacked: true }}
        curve="monotoneX"
        axisTop={null}
        axisRight={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          format: '.2s',
          legend: '',
          legendOffset: 0
        }}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          format: '.2f',
          legend: 'Days after ' + stockData.Date[0],
          legendOffset: 36,
          legendPosition: 'middle'
        }}
        axisLeft={{

          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          format: '.2s',
          legend: 'Opening price',
          legendOffset: -40,
          legendPosition: 'middle'
        }}
        enableGridX={false}
        colors={{ scheme: 'spectral' }}
        lineWidth={1}
        pointSize={4}
        pointColor={{ theme: 'background' }}
        pointBorderWidth={1}
        pointBorderColor={{ from: 'serieColor' }}
        enablePointLabel={false}
        pointLabel="y"
        pointLabelYOffset={-12}
        useMesh={true}
        legends={[
          {
            anchor: 'bottom-right',
            direction: 'column',
            justify: false,
            translateX: 140,
            translateY: 0,
            itemsSpacing: 2,
            itemDirection: 'left-to-right',
            itemWidth: 80,
            itemHeight: 12,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: 'circle',
            symbolBorderColor: 'rgba(0, 0, 0, .5)',
            effects: [
              {
                on: 'hover',
                style: {
                  itemBackground: 'rgba(0, 0, 0, .03)',
                  itemOpacity: 1
                }
              }
            ]
          }
        ]}
      />
    );
  }


}
