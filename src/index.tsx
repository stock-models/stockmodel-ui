import * as React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import { App } from './components';
import * as serviceWorker from './serviceWorker';
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
document.body.classList.add('background-darkslategray');
ReactDOM.render(<App displayText={'Hello moto!'}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
